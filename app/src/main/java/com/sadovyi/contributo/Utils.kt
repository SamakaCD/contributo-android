package com.sadovyi.contributo

import android.util.Log
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.StringBuilder

fun l(vararg args: Any) {
	val stringBuilder = StringBuilder()
	for (arg in args) {
		stringBuilder.append(arg).append(" ")
	}
	Log.d("Contributo", stringBuilder.toString())
}

fun <T> Observable<T>.applyNetworkSchedulers(): Observable<T> {
	return this.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.toCompletable() = this.flatMap { Completable.complete().toObservable<Nothing>() }!!