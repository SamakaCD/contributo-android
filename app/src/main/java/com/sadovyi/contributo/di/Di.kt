package com.sadovyi.contributo.di

import android.content.Context

private var sAppComponent: AppComponent? = null

fun buildDependencyGraph(applicationContext: Context) {
    sAppComponent = DaggerAppComponent.builder()
		    .appModule(AppModule(applicationContext))
            .build()
}

val appComponent: AppComponent
    get() {
        if (sAppComponent == null) {
            throw IllegalStateException("AppComponents is not initialized")
        }

        return sAppComponent!!
    }