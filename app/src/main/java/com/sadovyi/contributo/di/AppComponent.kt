package com.sadovyi.contributo.di

import com.sadovyi.contributo.presentation.MainActivity
import com.sadovyi.contributo.presentation.addproject.AddProjectActivity
import com.sadovyi.contributo.presentation.projects.ProjectsActivity
import com.sadovyi.contributo.presentation.projects.all.AllProjectsFragment
import com.sadovyi.contributo.presentation.projects.contributed.ContributedProjectsFragment
import com.sadovyi.contributo.presentation.projects.liked.LikedProjectsFragment
import com.sadovyi.contributo.presentation.projects.my.MyProjectsFragment
import com.sadovyi.contributo.presentation.signin.SignInActivity
import com.sadovyi.contributo.presentation.signup.SignUpActivity
import com.sadovyi.contributo.presentation.welcome.WelcomeActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class))
interface AppComponent {

	fun inject(mainActivity: MainActivity)
    fun inject(welcomeFragment: WelcomeActivity)
	fun inject(signInActivity: SignInActivity)
	fun inject(projectsActivity: ProjectsActivity)
	fun inject(addProjectActivity: AddProjectActivity)
	fun inject(signUpActivity: SignUpActivity)
    fun inject(allProjectsFragment: AllProjectsFragment)
    fun inject(myProjectsFragment: MyProjectsFragment)
    fun inject(likedProjectsFragment: LikedProjectsFragment)
    fun inject(contributedProjectsFragment: ContributedProjectsFragment)

}