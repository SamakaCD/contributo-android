package com.sadovyi.contributo.di

import android.content.Context
import com.sadovyi.contributo.data.Preferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

	@Provides
	fun provideApplicationContext() = context

	@Provides
	@Singleton
	fun providePreferences() = Preferences(context)

}
