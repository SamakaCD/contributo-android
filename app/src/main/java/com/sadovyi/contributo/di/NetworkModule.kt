package com.sadovyi.contributo.di

import com.sadovyi.contributo.data.api.ApiProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

	@Provides
	@Singleton
	fun provideApi() = ApiProvider.provideApi()

}
