package com.sadovyi.contributo.data.api.models

import com.google.gson.annotations.SerializedName
import com.sadovyi.contributo.entity.Project

data class ApiProject(
        val id: Int,
        @SerializedName("user_id") val userId: Int,
        val name: String,
        val description: String,
        val complexity: Int,
        val language: String?,
        @SerializedName("source_code_url") val sourceCodeUrl: String?,
        @SerializedName("is_contibutor") val isContributor: Boolean,
        @SerializedName("is_fan") val isFan: Boolean
) {

	fun toProject(): Project = Project(id, userId, name, description, complexity, language, isContributor, isFan, sourceCodeUrl)

}
