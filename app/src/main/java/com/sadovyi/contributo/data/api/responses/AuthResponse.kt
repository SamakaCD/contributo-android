package com.sadovyi.contributo.data.api.responses

import com.google.gson.annotations.SerializedName

data class AuthResponse(
		@SerializedName("token") val accessToken: String
)