package com.sadovyi.contributo.data.api

import com.sadovyi.contributo.data.api.models.ApiProfile
import com.sadovyi.contributo.data.api.models.ApiProject
import com.sadovyi.contributo.data.api.responses.AuthResponse
import io.reactivex.Observable
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*

interface Api {

	@POST("/sign_in")
	fun signIn(
			@Query("email") email: String,
			@Query("password") password: String
	): Observable<AuthResponse>

	@POST("/sign_up")
	fun signUp(
			@Query("email") email: String,
			@Query("password") password: String
	): Observable<AuthResponse>

    @PUT("/users/{id}")
    @FormUrlEncoded
    fun setUserName(
            @Field("name") name: String,
            @Path("id") userId: Int,
            @Header("access_token") accessToken: String
    ): Observable<ResponseBody>

    @PUT("/users/{id}")
    @Multipart
    fun uploadAvatar(
            @Part("avatar\"; filename=\"doge.jpg\"") avatar: RequestBody,
            @Path("id") userId: Int,
            @Header("access_token") accessToken: String
    ): Observable<ResponseBody>

    @GET("/projects")
	fun fetchAllProjects(
			@Header("access_token") accessToken: String
	): Observable<List<ApiProject>>

    @GET("/users/{id}/projects")
    fun fetchUserProjects(
            @Path("id") userId: Int,
            @Header("access_token") accessToken: String
    ): Observable<List<ApiProject>>


    @GET("/profile")
	fun fetchProfile(
			@Header("access_token") accessToken: String
	): Observable<ApiProfile>

	@POST("/projects")
	@FormUrlEncoded
	fun addProject(
			@Field("name") name: String,
			@Field("description") description: String,
			@Field("source_code_url") sourceUrl: String,
			@Field("language") language: String,
			@Field("complexity") complexity: Int,
			@Header("access_token") accessToken: String
	): Observable<ResponseBody>

	@POST("/projects/{id}/like")
	fun toggleLike(
			@Path("id") projectId: Int,
			@Header("access_token") accessToken: String
	): Observable<ResponseBody>

	@POST("/projects/{id}/contribute")
	fun toggleContribute(
			@Path("id") projectId: Int,
			@Header("access_token") accessToken: String
	): Observable<ResponseBody>

	@GET("/users/{id}/likes")
	fun getUserLikes(
			@Path("id") userId: Int,
			@Header("access_token") accessToken: String
	): Observable<List<ApiProject>>

	@GET("/users/{id}/contributions")
	fun getUserContributions(
			@Path("id") userId: Int,
			@Header("access_token") accessToken: String
	): Observable<List<ApiProject>>

}