package com.sadovyi.contributo.data.api.models

import com.sadovyi.contributo.entity.Profile

data class ApiProfile(val id: Int, val name: String) {

    fun toProfile() = Profile(id, name)

}