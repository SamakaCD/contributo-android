package com.sadovyi.contributo.data

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.sadovyi.contributo.entity.Profile

class Preferences(context: Context) {

	private val PREFERENCES_NAME = "preferences"
	private val ACCESS_TOKEN = "access_token"
    private val CURRENT_PROFILE = "current_profile"

	private val preferences: SharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)

	fun putAccessToken(accessToken: String) {
		preferences.edit()
				.putString(ACCESS_TOKEN, accessToken)
				.apply()
	}

	fun getAccessToken(): String? = preferences.getString(ACCESS_TOKEN, null)

    fun putCurrentProfile(user: Profile) {
        val userJson = Gson().toJson(user)
        preferences.edit()
                .putString(CURRENT_PROFILE, userJson)
                .apply()
    }

    fun getCurrentProfile(): Profile? {
        val userJson = preferences.getString(CURRENT_PROFILE, null)
        return Gson().fromJson<Profile>(userJson, Profile::class.java)
    }

	fun clear() {
		preferences.edit().clear().apply()
	}

}