package com.sadovyi.contributo.entity

data class Profile(val id: Int, val name: String)