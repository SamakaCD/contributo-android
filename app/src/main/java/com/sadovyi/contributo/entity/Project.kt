package com.sadovyi.contributo.entity

data class Project(
		val id: Int,
		val userId: Int,
		val name: String,
		val description: String,
		val complexity: Int,
		val language: String?,
		var isContributor: Boolean,
		var isFan: Boolean,
        var sourceCodeUrl: String?
)