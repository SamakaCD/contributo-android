package com.sadovyi.contributo.domain

import com.sadovyi.contributo.applyNetworkSchedulers
import com.sadovyi.contributo.data.Preferences
import com.sadovyi.contributo.data.api.Api
import com.sadovyi.contributo.entity.Project
import com.sadovyi.contributo.toCompletable
import io.reactivex.Observable
import io.reactivex.rxkotlin.toObservable
import javax.inject.Inject

class ApiInteractor @Inject constructor() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var preferences: Preferences

    fun allProjects(): Observable<List<Project>> {
        return api.fetchAllProjects(preferences.getAccessToken()!!)
                .applyNetworkSchedulers()
                .flatMap { it.toObservable() }
                .map { it.toProject() }
                .toList()
                .toObservable()
    }

    fun myProjects(): Observable<List<Project>> {
        return api.fetchUserProjects(preferences.getCurrentProfile()!!.id, preferences.getAccessToken()!!)
                .applyNetworkSchedulers()
                .flatMap { it.toObservable() }
                .map { it.toProject() }
                .toList()
                .toObservable()
    }

    fun likedProjects(): Observable<List<Project>> {
        return api.getUserLikes(preferences.getCurrentProfile()!!.id, preferences.getAccessToken()!!)
                .applyNetworkSchedulers()
                .flatMap { it.toObservable() }
                .map { it.toProject() }
                .doOnNext { it.isFan = true }
                .toList()
                .toObservable()
    }

    fun contributedProjects(): Observable<List<Project>> {
        return api.getUserContributions(preferences.getCurrentProfile()!!.id, preferences.getAccessToken()!!)
                .applyNetworkSchedulers()
                .flatMap { it.toObservable() }
                .map { it.toProject() }
                .doOnNext { it.isContributor = true }
                .toList()
                .toObservable()
    }

    fun toggleLike(project: Project): Observable<Nothing> {
        project.isFan = !project.isFan
        return api.toggleLike(project.id, preferences.getAccessToken()!!)
                .applyNetworkSchedulers()
                .toCompletable()
    }

    fun toggleContribution(project: Project): Observable<Nothing> {
        project.isContributor = !project.isContributor
        return api.toggleContribute(project.id, preferences.getAccessToken()!!)
                .applyNetworkSchedulers()
                .toCompletable()
    }

}