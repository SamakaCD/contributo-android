package com.sadovyi.contributo.domain

import android.content.Context
import android.net.Uri
import com.sadovyi.contributo.applyNetworkSchedulers
import com.sadovyi.contributo.data.Preferences
import com.sadovyi.contributo.data.api.Api
import com.sadovyi.contributo.entity.Profile
import com.sadovyi.contributo.toCompletable
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.RequestBody
import javax.inject.Inject

class SignUpInteractor @Inject constructor() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var context: Context

    lateinit var accessToken: String
    lateinit var profile: Profile

    fun signUp(email: String, password: String): Observable<Nothing> {
        return api.signUp(email, password)
                .applyNetworkSchedulers()
                .doOnNext { accessToken = it.accessToken }
                .flatMap { api.fetchProfile(accessToken).applyNetworkSchedulers() }
                .doOnNext { profile = it.toProfile() }
                .toCompletable()
    }

    fun setProfileName(name: String): Observable<Nothing> {
        return api.setUserName(name, profile.id, accessToken)
                .applyNetworkSchedulers()
                .toCompletable()
    }

    fun uploadAvatar(uri: Uri): Observable<Nothing> {
        return Observable.fromCallable { context.contentResolver.openInputStream(uri).readBytes() }
                .applyNetworkSchedulers()
                .flatMap { avatarBytes ->
                    val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), avatarBytes)
                    api.uploadAvatar(requestFile, profile.id, accessToken).applyNetworkSchedulers()
                }
                .doOnNext { saveSession() }
                .toCompletable()
    }

    fun saveSession() {
        preferences.putAccessToken(accessToken)
        preferences.putCurrentProfile(profile)
    }

}
