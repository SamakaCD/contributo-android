package com.sadovyi.contributo.domain

val genericException = Throwable("Something went wrong. Please, retry.")