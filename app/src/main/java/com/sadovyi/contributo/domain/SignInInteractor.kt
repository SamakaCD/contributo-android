package com.sadovyi.contributo.domain

import com.sadovyi.contributo.applyNetworkSchedulers
import com.sadovyi.contributo.data.Preferences
import com.sadovyi.contributo.data.api.Api
import com.sadovyi.contributo.l
import com.sadovyi.contributo.toCompletable
import io.reactivex.Observable
import io.reactivex.functions.Function
import retrofit2.HttpException
import javax.inject.Inject

class SignInInteractor @Inject constructor() {

	@Inject
	lateinit var api: Api

	@Inject
	lateinit var preferences: Preferences

	fun signIn(email: String, password: String): Observable<Nothing> {
		return api.signIn(email, password)
				.applyNetworkSchedulers()
				.map { it.accessToken }
				.doOnNext { l("Got token: $it") }
				.doOnNext { preferences.putAccessToken(it) }
				.flatMap { api.fetchProfile(it).applyNetworkSchedulers() }
                .doOnNext { preferences.putCurrentProfile(it.toProfile()) }
				.toCompletable()
				.onErrorResumeNext(Function<Throwable, Observable<Nothing>> { this@SignInInteractor.mapException(it) })
	}

	fun mapException(throwable: Throwable): Observable<Nothing> {
		if (throwable is HttpException && throwable.code() == 403) {
			return Observable.error(Throwable("Incorrect email or password."))
		}

		return Observable.error(genericException)
	}

}
