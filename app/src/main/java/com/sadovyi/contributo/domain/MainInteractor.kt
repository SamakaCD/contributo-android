package com.sadovyi.contributo.domain

import com.sadovyi.contributo.data.Preferences
import javax.inject.Inject

class MainInteractor @Inject constructor() {

    @Inject
    lateinit var preferences: Preferences

    fun signOut() {
        preferences.clear()
    }

}