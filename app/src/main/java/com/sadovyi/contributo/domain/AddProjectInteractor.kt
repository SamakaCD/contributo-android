package com.sadovyi.contributo.domain

import com.sadovyi.contributo.applyNetworkSchedulers
import com.sadovyi.contributo.data.Preferences
import com.sadovyi.contributo.data.api.Api
import com.sadovyi.contributo.toCompletable
import io.reactivex.Observable
import javax.inject.Inject

class AddProjectInteractor @Inject constructor() {

	@Inject
	lateinit var api: Api

	@Inject
	lateinit var preferences: Preferences

	fun saveProject(name: String, description: String, sourceUrl: String, language: String, complexity: Int): Observable<Nothing> {
		return api.addProject(name, description, sourceUrl, language, complexity, preferences.getAccessToken()!!)
				.applyNetworkSchedulers()
				.toCompletable()
	}

}
