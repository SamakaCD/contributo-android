package com.sadovyi.contributo.presentation.projects.contributed

import com.sadovyi.contributo.R
import com.sadovyi.contributo.di.appComponent
import com.sadovyi.contributo.presentation.common.Layout
import com.sadovyi.contributo.presentation.projects.common.BaseProjectsFragment
import javax.inject.Inject

@Layout(R.layout.fragment_grid)
class ContributedProjectsFragment : BaseProjectsFragment<ContributedProjectsPresenter>() {

    @Inject
    override lateinit var presenter: ContributedProjectsPresenter

    override fun inject() {
        appComponent.inject(this)
        presenter.view = this
    }

}