package com.sadovyi.contributo.presentation.signup

import com.sadovyi.contributo.presentation.common.BaseView

interface SignUpView : BaseView {

	fun showProgressBar()
	fun hideProgressBar()
    fun navigateToSetNameScreen()
    fun navigateToPickAvatarScreen()
	fun navigateToAllProjectsScreen()
	fun getEmail(): String
	fun getPassword(): String
    fun getName(): String

}