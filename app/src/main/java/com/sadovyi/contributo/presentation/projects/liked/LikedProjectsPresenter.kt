package com.sadovyi.contributo.presentation.projects.liked

import com.sadovyi.contributo.domain.ApiInteractor
import com.sadovyi.contributo.presentation.projects.common.BaseProjectsPresenter
import javax.inject.Inject

class LikedProjectsPresenter : BaseProjectsPresenter {

    @Suppress("ConvertSecondaryConstructorToPrimary")
    @Inject
    constructor(interactor: ApiInteractor) : super(interactor)

    @Inject
    lateinit var interactor: ApiInteractor

    override fun projects() = interactor.likedProjects()

}