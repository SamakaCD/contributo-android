package com.sadovyi.contributo.presentation.projects

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.PopupMenu
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import butterknife.OnClick
import com.sadovyi.contributo.R
import com.sadovyi.contributo.di.appComponent
import com.sadovyi.contributo.presentation.addproject.AddProjectActivity
import com.sadovyi.contributo.presentation.common.BaseActivity
import com.sadovyi.contributo.presentation.common.Layout
import com.sadovyi.contributo.presentation.profile.ProfileActivity
import com.sadovyi.contributo.presentation.welcome.WelcomeActivity
import kotlinx.android.synthetic.main.screen_projects.*
import javax.inject.Inject

@Layout(R.layout.screen_projects)
class ProjectsActivity : BaseActivity(), ProjectsView {

    @Inject
    lateinit var presenter: ProjectsPresenter

    private val TABS_COUNT = 4

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        presenter.view = this
        viewPager.adapter = ViewPagerAdapter(supportFragmentManager)
        viewPager.offscreenPageLimit = TABS_COUNT
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun navigateToWelcomeScreenAndFinish() {
        finish()
        startActivity(Intent(this, WelcomeActivity::class.java))
    }

    fun navigateToAddProjectActivity() {
        startActivity(Intent(this, AddProjectActivity::class.java))
    }

    fun navigateToProfileActivity() {
        startActivity(Intent(this, ProfileActivity::class.java))
    }

    fun showSignOutConfirmationDialog() {
        AlertDialog.Builder(this)
                .setMessage("Do you want to sign out?")
                .setPositiveButton("Sign out", { _, _ -> presenter.signOut() })
                .setNegativeButton("Cancel", { _, _ -> })
                .show()
    }

    fun showSearch() {
        search.animate()
                .setDuration(2000)
                .setInterpolator(DecelerateInterpolator(7f))
                .translationY(0f)
                .start()

        searchBottom.animate()
                .setDuration(2000)
                .setInterpolator(DecelerateInterpolator(7f))
                .translationY(0f)
                .start()

        fabAddProject.hide()
    }

    fun hideSearch() {
        search.animate()
                .setDuration(300)
                .setInterpolator(AccelerateInterpolator())
                .translationY(-112.dp)
                .start()

        searchBottom.animate()
                .setDuration(300)
                .setInterpolator(AccelerateInterpolator())
                .translationY(56.dp)
                .start()

        fabAddProject.show()
    }

    @OnClick(R.id.menuButton)
    fun onMenuClick() {
        val popupMenu = PopupMenu(this, menuButton)
        popupMenu.inflate(R.menu.all_projects_menu)
        popupMenu.setOnMenuItemClickListener { onMenuItemSelected(it.itemId) }
        popupMenu.show()
    }

    @OnClick(R.id.fabAddProject)
    fun onAddProjectClick() {
        navigateToAddProjectActivity()
    }

    @OnClick(R.id.profile)
    fun onProfileClick() {
        navigateToProfileActivity()
    }

    @OnClick(R.id.searchButton)
    fun onSearchClick() {
        showSearch()
    }

    @OnClick(R.id.close)
    fun onCloseSearchClick() {
        hideSearch()
    }

    fun onMenuItemSelected(itemId: Int): Boolean {
        when(itemId) {
            R.id.signOut -> showSignOutConfirmationDialog()
        }
        return true
    }

}