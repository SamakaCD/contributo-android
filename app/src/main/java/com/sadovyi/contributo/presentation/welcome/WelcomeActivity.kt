package com.sadovyi.contributo.presentation.welcome

import android.content.Intent
import android.os.Bundle
import butterknife.OnClick
import com.sadovyi.contributo.R
import com.sadovyi.contributo.di.appComponent
import com.sadovyi.contributo.presentation.common.BaseActivity
import com.sadovyi.contributo.presentation.common.Layout
import com.sadovyi.contributo.presentation.signin.SignInActivity
import com.sadovyi.contributo.presentation.signup.SignUpActivity

@Layout(R.layout.screen_welcome)
class WelcomeActivity : BaseActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		appComponent.inject(this)
	}

	@OnClick(R.id.buttonSignIn)
	fun onSignInClick() = startActivity(Intent(this, SignInActivity::class.java))

	@OnClick(R.id.buttonSignUp)
	fun onSignUpClick() = startActivity(Intent(this, SignUpActivity::class.java))

}
