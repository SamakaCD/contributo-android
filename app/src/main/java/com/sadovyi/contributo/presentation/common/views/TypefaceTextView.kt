package com.sadovyi.contributo.presentation.common.views

import android.content.Context
import android.graphics.Typeface
import android.support.annotation.StringRes
import android.util.AttributeSet
import android.util.Log
import android.widget.TextView
import com.sadovyi.contributo.R


class TypefaceTextView(context: Context, attrs: AttributeSet) : TextView(context, attrs) {

    private val TAG = this.javaClass.simpleName

    init {
        this.initialize(context, attrs)
    }

    fun initialize(context: Context, attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.TypefaceTextView)
        if (typedArray.hasValue(R.styleable.TypefaceTextView_font))
            setAssetFont(typedArray.getString(R.styleable.TypefaceTextView_font))
        typedArray.recycle()
    }

    fun setAssetFont(assetFont: String) {
        val typeface: Typeface
        try {
            typeface = Typeface.createFromAsset(context.assets, assetFont)
            setTypeface(typeface)
        } catch (e: Exception) {
            Log.e(TAG, "Unable to load typeface: " + e.message)
        }

    }

    fun setFont(@StringRes fontName: Int) {
        val font = context.getString(fontName)
        setAssetFont(font)
    }

}