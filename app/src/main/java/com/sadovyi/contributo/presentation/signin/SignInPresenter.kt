package com.sadovyi.contributo.presentation.signin

import com.sadovyi.contributo.domain.SignInInteractor
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SignInPresenter @Inject constructor() {

	@Inject
	lateinit var interactor: SignInInteractor

	lateinit var view: SignInView

	fun signIn() {
		view.showProgressBar()
		interactor.signIn(view.getEmail(), view.getPassword())
				.subscribeBy(
						onComplete = { view.navigateToAllProjectsScreen() },
						onError = {
                            it.printStackTrace()
							view.hideProgressBar()
							view.showErrorMessage(it.message!!)
						}
				)
	}

}
