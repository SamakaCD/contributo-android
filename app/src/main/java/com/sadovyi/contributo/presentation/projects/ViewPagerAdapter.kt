package com.sadovyi.contributo.presentation.projects

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.sadovyi.contributo.presentation.projects.all.AllProjectsFragment
import com.sadovyi.contributo.presentation.projects.contributed.ContributedProjectsFragment
import com.sadovyi.contributo.presentation.projects.liked.LikedProjectsFragment
import com.sadovyi.contributo.presentation.projects.my.MyProjectsFragment

class ViewPagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {

    private val PAGES_TITLES = arrayOf("All", "My", "Liked", "Contributed")
    private val PAGES_FRAGMENTS = arrayOf(
            AllProjectsFragment(),
            MyProjectsFragment(),
            LikedProjectsFragment(),
            ContributedProjectsFragment()
    )

    override fun getItem(position: Int) = PAGES_FRAGMENTS[position]

    override fun getCount() = PAGES_TITLES.size

    override fun getPageTitle(position: Int) = PAGES_TITLES[position]
}