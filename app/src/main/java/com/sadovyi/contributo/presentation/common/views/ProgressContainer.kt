package com.sadovyi.contributo.presentation.common.views

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar

class ProgressContainer : FrameLayout {

	private val childLayout = FrameLayout(context)
	private val progressBar = ProgressBar(context)
	private var isInitializing = true

	constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

	constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
			super(context, attrs, defStyleAttr)

	init {
		addView(childLayout, FrameLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
		addView(progressBar, FrameLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER))
		isInitializing = false

		progressBar.alpha = 0f
		progressBar.visibility = View.GONE
	}

	override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
		if (isInitializing) {
			super.addView(child, index, params)
		} else {
			childLayout.addView(child, index, params)
		}
	}

	fun showProgressBar() {
		childLayout.animate()
				.setDuration(300)
				.alpha(0f)
				.withEndAction { childLayout.visibility = View.INVISIBLE }
				.start()

		progressBar.visibility = View.VISIBLE
		progressBar.animate()
				.setDuration(300)
				.alpha(1f)
				.start()
	}

	fun hideProgressBar() {
		childLayout.visibility = View.VISIBLE
		childLayout.animate()
				.setDuration(300)
				.alpha(1f)
				.start()

		progressBar.animate()
				.setDuration(300)
				.alpha(0f)
				.withEndAction { progressBar.visibility = View.GONE }
				.start()
	}

}