package com.sadovyi.contributo.presentation.signin

import com.sadovyi.contributo.presentation.common.BaseView

interface SignInView: BaseView {

	fun showProgressBar()
	fun hideProgressBar()
	fun navigateToAllProjectsScreen()
	fun getEmail(): String
	fun getPassword(): String

}