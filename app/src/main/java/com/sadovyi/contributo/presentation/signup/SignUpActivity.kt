package com.sadovyi.contributo.presentation.signup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.view.PagerAdapter
import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import butterknife.OnClick
import com.sadovyi.contributo.R
import com.sadovyi.contributo.di.appComponent
import com.sadovyi.contributo.presentation.common.BaseActivity
import com.sadovyi.contributo.presentation.common.Layout
import com.sadovyi.contributo.presentation.projects.ProjectsActivity
import kotlinx.android.synthetic.main.include_sign_up_credentials.*
import kotlinx.android.synthetic.main.include_sign_up_name.*
import kotlinx.android.synthetic.main.screen_sign_up.*
import javax.inject.Inject

@Layout(R.layout.screen_sign_up)
class SignUpActivity : BaseActivity(), SignUpView {

    private val PICK_AVATAR_REQUEST = 1

	@Inject
	lateinit var presenter: SignUpPresenter

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		appComponent.inject(this)
		presenter.view = this
        pager.offscreenPageLimit = 3
        pager.adapter = pagerAdapter
        pager.post { findViewById(R.id.pickAvatarButton).setOnClickListener { this.onPickAvatarClick() } }
	}

	override fun showProgressBar() {
        hideKeyboard()
        progressContainer.showProgressBar()
    }

	override fun hideProgressBar() = progressContainer.hideProgressBar()

    override fun navigateToSetNameScreen() {
        pager.currentItem = 1
    }

    override fun navigateToPickAvatarScreen() {
        pager.currentItem = 2
        navigationButtonsContainer.animate()
                .setDuration(300)
                .setInterpolator(FastOutLinearInInterpolator())
                .translationY(56.dp)
                .start()
    }

    override fun navigateToAllProjectsScreen() {
        val intent = Intent(this, ProjectsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    override fun getEmail() = emailEditText.text.toString()

	override fun getPassword() = passwordEditText.text.toString()

    override fun getName() = nameEditText.text.toString()

    @OnClick(R.id.nextButton)
    fun onNextClick() {
        when (pager.currentItem) {
            0 -> presenter.signUp()
            1 -> presenter.setName()
        }
    }

    fun onPickAvatarClick() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(photoPickerIntent, PICK_AVATAR_REQUEST)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }

        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_AVATAR_REQUEST && resultCode == Activity.RESULT_OK) {
            presenter.uploadAvatar(data!!.data)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    val pagerAdapter = object : PagerAdapter() {

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            var layout = findViewById(0)
            when (position) {
                0 -> layout = layoutInflater.inflate(R.layout.include_sign_up_credentials, null)
                1 -> layout = layoutInflater.inflate(R.layout.include_sign_up_name, null)
                2 -> layout = layoutInflater.inflate(R.layout.include_sign_up_avatar, null)
            }
            container.addView(layout)
            return layout
        }

        override fun destroyItem(container: ViewGroup, position: Int, obj: Any?) {
            container.removeView(obj as View)
        }

        override fun isViewFromObject(view: View?, obj: Any?) = view == obj

        override fun getCount() = 3

    }

}
