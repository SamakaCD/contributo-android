package com.sadovyi.contributo.presentation.signin

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import butterknife.OnClick
import com.sadovyi.contributo.R
import com.sadovyi.contributo.di.appComponent
import com.sadovyi.contributo.presentation.common.BaseActivity
import com.sadovyi.contributo.presentation.common.Layout
import com.sadovyi.contributo.presentation.projects.ProjectsActivity
import kotlinx.android.synthetic.main.screen_sign_in.*
import javax.inject.Inject

@Layout(R.layout.screen_sign_in)
class SignInActivity : BaseActivity(), SignInView {

	@Inject
	lateinit var presenter: SignInPresenter

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setupToolbar()
		appComponent.inject(this)
		presenter.view = this
		passwordEditText.setOnEditorActionListener { _, actionId, _ ->
			if (actionId == EditorInfo.IME_ACTION_GO) {
                hideKeyboard()
				presenter.signIn()
			}
			true
		}
	}

	override fun showProgressBar() = progressContainer.showProgressBar()

	override fun hideProgressBar() = progressContainer.hideProgressBar()

	override fun navigateToAllProjectsScreen() {
		val intent = Intent(this, ProjectsActivity::class.java)
		intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
		startActivity(intent)
	}

	override fun getEmail(): String = emailEditText.text.toString()

	override fun getPassword(): String = passwordEditText.text.toString()

	@OnClick(R.id.buttonSignIn)
	fun onSignInButtonClick() {
        hideKeyboard()
        presenter.signIn()
    }

	fun setupToolbar() {
		supportActionBar!!.setDisplayHomeAsUpEnabled(true)
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			android.R.id.home -> finish()
		}

		return false
	}

}
