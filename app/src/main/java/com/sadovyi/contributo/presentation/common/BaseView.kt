package com.sadovyi.contributo.presentation.common

interface BaseView {

	fun showErrorMessage(errorMessage: String = "Something went wrong. Please, retry")

}
