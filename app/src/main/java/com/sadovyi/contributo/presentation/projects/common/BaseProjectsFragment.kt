package com.sadovyi.contributo.presentation.projects.common

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.google.gson.Gson
import com.sadovyi.contributo.R
import com.sadovyi.contributo.entity.Project
import com.sadovyi.contributo.presentation.common.BaseFragment
import com.sadovyi.contributo.presentation.details.ProjectDetailsActivity

abstract class BaseProjectsFragment<P : BaseProjectsPresenter> : BaseFragment(), BaseProjectsView {

    @BindView(R.id.recyclerView)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.swipeRefreshLayout)
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    @BindView(R.id.emptyStateContainer)
    lateinit var emptyStateContainer: View

    abstract var presenter: P
    abstract fun inject()

    private val adapter by lazy { ProjectsAdapter(activity, presenter) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val contentView = inflater!!.inflate(R.layout.fragment_grid, null)
        ButterKnife.bind(this, contentView)
        inject()
        setupGrid()
        setupSwipeRefreshLayout()
        presenter.onInit()
        return contentView
    }

    override fun showErrorMessage(errorMessage: String) {
        Snackbar.make(view!!, errorMessage, Snackbar.LENGTH_SHORT).show()
    }

    override fun showProgressBar() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideProgressBar() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showEmptyState() {
        emptyStateContainer.visibility = View.VISIBLE
    }

    override fun hideEmptyState() {
        emptyStateContainer.visibility = View.GONE
    }

    override fun invalidate() {
        adapter.notifyDataSetChanged()
    }

    override fun navigateToProjectDetails(project: Project, activityOptions: Bundle) {
        val intent = Intent(activity, ProjectDetailsActivity::class.java)
        intent.putExtra(Intent.EXTRA_INTENT, Gson().toJson(project))
        startActivity(intent, activityOptions)
    }

    override fun setProjects(projects: List<Project>) = adapter.setProjects(projects)

    fun setupGrid() {
        val columnsCount = Math.floor((resources.displayMetrics.widthPixels / 256.dp).toDouble()).toInt()
        recyclerView.layoutManager = StaggeredGridLayoutManager(columnsCount, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.adapter = adapter
    }

    fun setupSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent)
        swipeRefreshLayout.setOnRefreshListener { presenter.refresh() }
    }

}