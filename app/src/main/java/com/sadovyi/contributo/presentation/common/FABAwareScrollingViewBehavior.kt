package com.sadovyi.contributo.presentation.common

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.view.ViewCompat
import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.util.AttributeSet
import android.view.View


class FABAwareScrollingViewBehavior(context: Context, attrs: AttributeSet) : CoordinatorLayout.Behavior<FloatingActionButton>() {

    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout?, child: FloatingActionButton, target: View?, dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed)
        if (dyConsumed > 0) {
            animateFabHide(child)
        } else if (dyConsumed < 0) {
            animateFabShow(child)
        }
    }

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout?, child: FloatingActionButton?, directTargetChild: View?, target: View?, nestedScrollAxes: Int): Boolean {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL
    }

    fun animateFabShow(fabView: View) {
        fabView.animate()
                .setDuration(150)
                .setInterpolator(LinearOutSlowInInterpolator())
                .translationY(0f)
                .start()
    }

    fun animateFabHide(fabView: View) {
        val layoutParams = fabView.layoutParams as CoordinatorLayout.LayoutParams
        val bottomMargin = layoutParams.bottomMargin
        fabView.animate()
                .setDuration(150)
                .setInterpolator(FastOutLinearInInterpolator())
                .translationY((fabView.height + bottomMargin).toFloat())
                .start()
    }

}