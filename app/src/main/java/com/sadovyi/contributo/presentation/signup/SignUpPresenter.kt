package com.sadovyi.contributo.presentation.signup

import android.net.Uri
import com.sadovyi.contributo.domain.SignUpInteractor
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SignUpPresenter @Inject constructor() {

	@Inject
	lateinit var interactor: SignUpInteractor

	lateinit var view: SignUpView

	fun signUp() {
		view.showProgressBar()
		interactor.signUp(view.getEmail(), view.getPassword())
				.subscribeBy(
						onComplete = {
                            view.hideProgressBar()
                            view.navigateToSetNameScreen()
                        },
						onError = {
                            it.printStackTrace()
							view.hideProgressBar()
							view.showErrorMessage()
						}
				)
	}

    fun setName() {
        view.showProgressBar()
        interactor.setProfileName(view.getName())
                .subscribeBy(
                        onComplete = {
                            view.hideProgressBar()
                            view.navigateToPickAvatarScreen()
                        },
                        onError = {
                            it.printStackTrace()
                            view.hideProgressBar()
                            view.showErrorMessage()
                        }
                )
    }

    fun uploadAvatar(uri: Uri) {
        view.showProgressBar()
        interactor.uploadAvatar(uri)
                .subscribeBy(
                        onComplete = {
                            view.navigateToAllProjectsScreen()
                        },
                        onError = {
                            it.printStackTrace()
                            view.showErrorMessage()
                            view.hideProgressBar()
                        }
                )
    }

}
