package com.sadovyi.contributo.presentation.common

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import butterknife.ButterKnife


open class BaseActivity : AppCompatActivity(), BaseView {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val layoutId = getLayoutId()
		if (layoutId != 0) {
			setContentView(layoutId)
			ButterKnife.bind(this)
		}
	}

	override fun showErrorMessage(errorMessage: String) {
		val contentView = findViewById(android.R.id.content)!!
		Snackbar.make(contentView, errorMessage, Snackbar.LENGTH_SHORT).show()
	}

	private fun getLayoutId(): Int {
		val layoutAnnotation = javaClass.getAnnotation(Layout::class.java)
		if (layoutAnnotation != null) {
			return layoutAnnotation.value
		} else {
			return 0
		}
	}

    fun hideKeyboard() {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

	val Int.dp: Float
		get() = resources.displayMetrics.density * this

}
