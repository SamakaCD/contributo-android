package com.sadovyi.contributo.presentation.projects.common

import android.app.Activity
import android.content.Context
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageButton
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.sadovyi.contributo.R
import com.sadovyi.contributo.entity.Project

class ProjectsAdapter : RecyclerView.Adapter<ProjectsAdapter.ViewHolder> {

	val COMPLEXITY_LEVELS = arrayOf("Easy", "Normal", "Medium", "Complicated", "Hard")

	val context: Context
	val presenter: BaseProjectsPresenter
	var items = emptyList<Project>()

	@Suppress("ConvertSecondaryConstructorToPrimary")
	constructor(context: Context, presenter: BaseProjectsPresenter): super() {
		this.context = context
		this.presenter = presenter
	}

	fun setProjects(projects: List<Project>) {
        this.items = projects
        notifyDataSetChanged()
    }

	override fun getItemCount(): Int = items.size

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val layoutInflater = LayoutInflater.from(context)
		val itemView = layoutInflater.inflate(R.layout.item_project, parent, false)
		return ViewHolder(itemView)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val project = items[position]
		holder.itemView.tag = project
		holder.title.text = project.name
		holder.description.text = project.description
		holder.language.text = project.language ?: "Unspecified"
		holder.complexity.text = "/${COMPLEXITY_LEVELS[Math.min(0, Math.max(5, project.complexity - 1))]}"

        if (project.isContributor) {
            holder.contributeButton.setImageResource(R.drawable.ic_fork_selected)
        } else {
            holder.contributeButton.setImageResource(R.drawable.ic_fork_grey)
        }

        if (project.isFan) {
            holder.likeButton.setImageResource(R.drawable.ic_heart_selected)
        } else {
            holder.likeButton.setImageResource(R.drawable.ic_heart_grey)
        }
	}

	inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

		@BindView(R.id.title) lateinit var title: TextView
		@BindView(R.id.description) lateinit var description: TextView
		@BindView(R.id.language) lateinit var language: TextView
		@BindView(R.id.complexity) lateinit var complexity: TextView
		@BindView(R.id.likeButton) lateinit var likeButton: ImageButton
		@BindView(R.id.contributeButton) lateinit var contributeButton: ImageButton

		init {
			ButterKnife.bind(this, itemView)
		}

		@OnClick(R.id.likeButton)
		fun onLikeClick() {
            presenter.toggleLike(itemView.tag as Project)
        }

		@OnClick(R.id.contributeButton)
		fun onContributeClick() {
            presenter.toggleContribution(itemView.tag as Project)
        }

        @OnClick(R.id.cardView)
        fun onCardClick() {
            val activity = context as Activity
            val viewPairs = mutableListOf<Pair<View, String>>(
                    Pair(itemView, "card"), Pair(title, "title"), Pair(description, "description"))

            activity.findViewById(android.R.id.statusBarBackground)?.let {
                viewPairs.add(Pair(it, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME))
            }

            activity.findViewById(android.R.id.navigationBarBackground)?.let {
                viewPairs.add(Pair(it, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME))
            }

            val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, *viewPairs.toTypedArray())
            presenter.openProjectDetails(itemView.tag as Project, activityOptions.toBundle())
        }

	}

}