package com.sadovyi.contributo.presentation.projects

import com.sadovyi.contributo.domain.MainInteractor
import javax.inject.Inject

class ProjectsPresenter @Inject constructor() {

    @Inject
    lateinit var interactor: MainInteractor

    lateinit var view: ProjectsView

    fun signOut() {
        interactor.signOut()
        view.navigateToWelcomeScreenAndFinish()
    }

}