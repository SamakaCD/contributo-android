package com.sadovyi.contributo.presentation.common

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife

open class BaseFragment : Fragment(), BaseView {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val layoutId = getLayoutId()
        if (layoutId != 0) {
	        val view = LayoutInflater.from(context).inflate(layoutId, null)
	        ButterKnife.bind(this, view)
            return view
        } else {
            return null
        }
    }

	override fun showErrorMessage(errorMessage: String) {
		Snackbar.make(view!!, errorMessage, Snackbar.LENGTH_SHORT).show()
	}

	private fun getLayoutId(): Int {
        val layoutAnnotation = javaClass.getAnnotation(Layout::class.java)
        if (layoutAnnotation != null) {
            return layoutAnnotation.value
        } else {
            return 0
        }
    }

	val Int.dp: Float
			get() = resources.displayMetrics.density * this

}