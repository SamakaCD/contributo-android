package com.sadovyi.contributo.presentation

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.sadovyi.contributo.data.Preferences
import com.sadovyi.contributo.di.appComponent
import com.sadovyi.contributo.di.buildDependencyGraph
import com.sadovyi.contributo.presentation.projects.ProjectsActivity
import com.sadovyi.contributo.presentation.welcome.WelcomeActivity
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

	@Inject
	lateinit var preferences: Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buildDependencyGraph(applicationContext)
	    appComponent.inject(this)

	    if (preferences.getAccessToken() != null) {
		    navigateToAllProjectsScreen()
	    } else {
		    navigateToWelcomeScreen()
	    }
    }

	fun navigateToWelcomeScreen() {
		startActivity(Intent(this, WelcomeActivity::class.java))
		finish()
	}

	fun navigateToAllProjectsScreen() {
		startActivity(Intent(this, ProjectsActivity::class.java))
		finish()
	}

}

