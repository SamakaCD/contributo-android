package com.sadovyi.contributo.presentation.addproject

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Patterns
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Toast
import com.sadovyi.contributo.R
import com.sadovyi.contributo.di.appComponent
import com.sadovyi.contributo.presentation.common.BaseActivity
import com.sadovyi.contributo.presentation.common.Layout
import kotlinx.android.synthetic.main.screen_add_project.*
import javax.inject.Inject

@Layout(R.layout.screen_add_project)
class AddProjectActivity : BaseActivity(), AddProjectView {

    @Inject
    lateinit var presenter: AddProjectPresenter

    var selectedComplexity = 2
    var canBeValidated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        presenter.view = this
        setupToolbar()
        setupLanguageSpinner()

        for (i in 0..2) {
            complexityViewsContainer.getChildAt(i).setOnClickListener {
                for (j in 0..2) {
                    complexityViewsContainer.getChildAt(j).setBackgroundResource(R.drawable.background_complexity)
                }
                complexityViewsContainer.getChildAt(i).setBackgroundResource(R.drawable.background_complexity_selected)
                selectedComplexity = i * 2
            }
        }

        projectNameEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) { }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateProjectName()
            }
        })

        projectDescriptionEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) { }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateDescription()
            }
        })

        sourceCodeUrlEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) { }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validateUrl()
            }
        })
    }

    override fun showProgressBar() = progressContainer.showProgressBar()

    override fun hideProgressBar() = progressContainer.hideProgressBar()

    override fun showSuccessToast() = Toast.makeText(this, "Project added", Toast.LENGTH_SHORT).show()

    override fun close() = finish()

    override fun getProjectName() = projectNameEditText.text.toString()

    override fun getProjectDescription() = projectDescriptionEditText.text.toString()

    override fun getUrl() = sourceCodeUrlEditText.text.toString()

    override fun getLanguage(): String {
        return resources.getStringArray(R.array.langs)[languageSpinner.selectedItemPosition]
    }

    override fun getComplexity() = selectedComplexity

    fun setupToolbar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    fun setupLanguageSpinner() {
        val languages = resources.getStringArray(R.array.langs)
        val adapter = ArrayAdapter(this, R.layout.item_spinner, languages)
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown)
        languageSpinner.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_project, menu)
        return true
    }

    fun validateProjectName(): Boolean {
        if (!canBeValidated) {
            return true
        }

        if (TextUtils.isEmpty(getProjectName())) {
            projectNameContainer.error = "Project name is required"
            return false
        } else {
            projectNameContainer.error = null
            return true
        }
    }

    fun validateDescription(): Boolean {
        if (!canBeValidated) {
            return true
        }

        if (TextUtils.isEmpty(getProjectDescription())) {
            descriptionContainer.error = "Project description is required"
            return false
        } else {
            descriptionContainer.error = null
            return true
        }
    }

    fun validateUrl(): Boolean {
        if (!canBeValidated) {
            return true
        }

        if (!TextUtils.isEmpty(getUrl()) && Patterns.WEB_URL.matcher(getUrl()).matches()) {
            sourceContainer.error = null
            return true
        } else {
            sourceContainer.error = "Source code URL is not valid"
            return false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.save -> {
                canBeValidated = true
                val ok = validateProjectName() and validateDescription() and validateUrl()
                if (ok) {
                    presenter.saveProject()
                }
            }
        }

        return false
    }

}
