package com.sadovyi.contributo.presentation.details

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import butterknife.OnClick
import com.google.gson.Gson
import com.sadovyi.contributo.R
import com.sadovyi.contributo.entity.Project
import com.sadovyi.contributo.presentation.common.BaseActivity
import com.sadovyi.contributo.presentation.common.Layout
import com.sadovyi.contributo.presentation.common.TransitionHelper
import kotlinx.android.synthetic.main.screen_project_detais.*

@Layout(R.layout.screen_project_detais)
class ProjectDetailsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TransitionHelper.fixSharedElementTransitionForStatusAndNavigationBar(this)
        val project = Gson().fromJson(intent.getStringExtra(Intent.EXTRA_INTENT), Project::class.java)
        name.text = project.name
        description.text = project.description
        project.sourceCodeUrl?.let {
            url.text = it
            url.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        }
    }

    @OnClick(R.id.back)
    fun onBackClick() {
        finishAfterTransition()
    }
}
