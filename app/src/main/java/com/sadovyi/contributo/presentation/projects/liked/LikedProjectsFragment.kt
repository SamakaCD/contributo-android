package com.sadovyi.contributo.presentation.projects.liked

import com.sadovyi.contributo.R
import com.sadovyi.contributo.di.appComponent
import com.sadovyi.contributo.presentation.common.Layout
import com.sadovyi.contributo.presentation.projects.common.BaseProjectsFragment
import javax.inject.Inject

@Layout(R.layout.fragment_grid)
class LikedProjectsFragment : BaseProjectsFragment<LikedProjectsPresenter>() {

    @Inject
    override lateinit var presenter: LikedProjectsPresenter

    override fun inject() {
        appComponent.inject(this)
        presenter.view = this
    }

}