package com.sadovyi.contributo.presentation.projects.common

import android.os.Bundle
import com.sadovyi.contributo.entity.Project
import com.sadovyi.contributo.presentation.common.BaseView

interface BaseProjectsView : BaseView {

    fun showProgressBar()
    fun hideProgressBar()
    fun showEmptyState()
    fun hideEmptyState()
    fun setProjects(projects: List<Project>)
    fun invalidate()
    fun navigateToProjectDetails(project: Project, activityOptions: Bundle)

}