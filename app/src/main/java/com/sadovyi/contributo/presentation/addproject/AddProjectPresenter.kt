package com.sadovyi.contributo.presentation.addproject

import com.sadovyi.contributo.domain.AddProjectInteractor
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class AddProjectPresenter @Inject constructor() {

	@Inject
	lateinit var interactor: AddProjectInteractor

	lateinit var view: AddProjectView

	fun saveProject() {
		view.showProgressBar()
		interactor.saveProject(view.getProjectName(), view.getProjectDescription(),
				view.getUrl(), view.getLanguage(), view.getComplexity())
				.subscribeBy(
						onComplete = {
							view.showSuccessToast()
							view.close()
						},
						onError = {
							view.hideProgressBar()
							view.showErrorMessage()
						}
				)
	}

}
