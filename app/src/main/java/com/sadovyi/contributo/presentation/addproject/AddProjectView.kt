package com.sadovyi.contributo.presentation.addproject

import com.sadovyi.contributo.presentation.common.BaseView

interface AddProjectView : BaseView {

	fun showProgressBar()
	fun hideProgressBar()
	fun showSuccessToast()
	fun close()
	fun getProjectName(): String
	fun getProjectDescription(): String
	fun getUrl(): String
	fun getLanguage(): String
	fun getComplexity(): Int

}
