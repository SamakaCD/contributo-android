package com.sadovyi.contributo.presentation.common.views

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent


class CustomViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    var isPagingEnabled = false

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (isPagingEnabled) {
            return super.onTouchEvent(event)
        }

        return false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if (isPagingEnabled) {
            return super.onInterceptTouchEvent(event)
        }

        return false
    }

}
