package com.sadovyi.contributo.presentation.projects.common

import android.os.Bundle
import com.sadovyi.contributo.domain.ApiInteractor
import com.sadovyi.contributo.entity.Project
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy

abstract class BaseProjectsPresenter(private val interactor: ApiInteractor) {

    abstract fun projects(): Observable<List<Project>>

    lateinit var view: BaseProjectsView

    fun onInit() = refresh()

    fun refresh() {
        view.showProgressBar()
        projects().subscribeBy(
                onNext = {
                    view.setProjects(it)
                    view.hideProgressBar()
                    if (it.isNotEmpty()) {
                        view.hideEmptyState()
                    } else {
                        view.showEmptyState()
                    }
                },
                onError = {
                    it.printStackTrace()
                    view.showErrorMessage()
                    view.hideProgressBar()
                }
        )
    }

    fun toggleLike(project: Project) {
        interactor.toggleLike(project).subscribeBy()
        view.invalidate()
    }

    fun toggleContribution(project: Project) {
        interactor.toggleContribution(project).subscribeBy()
        view.invalidate()
    }

    fun openProjectDetails(project: Project, activityOptions: Bundle) = view.navigateToProjectDetails(project, activityOptions)

}