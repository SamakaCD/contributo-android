package com.sadovyi.contributo.presentation.projects.all

import com.sadovyi.contributo.domain.ApiInteractor
import com.sadovyi.contributo.presentation.projects.common.BaseProjectsPresenter
import com.sadovyi.contributo.presentation.projects.common.BaseProjectsView
import javax.inject.Inject

class AllProjectsPresenter : BaseProjectsPresenter {

    @Suppress("ConvertSecondaryConstructorToPrimary")
    @Inject
    constructor(interactor: ApiInteractor) : super(interactor)

    @Inject
    lateinit var interactor: ApiInteractor

    override fun projects() = interactor.allProjects()

}