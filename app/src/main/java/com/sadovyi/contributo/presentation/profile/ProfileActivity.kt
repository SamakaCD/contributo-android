package com.sadovyi.contributo.presentation.profile

import android.os.Bundle
import com.sadovyi.contributo.R
import com.sadovyi.contributo.presentation.common.BaseActivity
import com.sadovyi.contributo.presentation.common.Layout

@Layout(R.layout.screen_profile)
class ProfileActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}
